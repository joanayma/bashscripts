#!/bin/bash
(

echo -e "\e[1m [$0] Services that are not fully deployed: \e[0m"
# get services names if n<m or n=0 in services replicas n/m (running/total)
docker service ls | awk 'int(substr($4,1,1)) == 0 { print }; int(substr($4,1,1)) < int(substr($4,3,3)) { print }'
services=$(docker service ls | awk ' int(substr($4,1,1)) < int(substr($4,3,3)) { print $2 }')
echo

if ! [ "$services" = "" ]; then
echo -e "\e[1m [$0] Services details: \e[0m"
for srv in $services; do
   echo -e "\e[1m [$0] Service $srv details: \e[0m"
   docker service ps --no-trunc $srv --no-trunc | head -3
   echo
done
echo -e "\e[1m [$0] Use \e[4mdocker service logs -f <service> \e[24m \e[0m"
echo
fi

echo -e "\e[1m [$0] Nodes that are not online: \e[0m"
docker node ls | grep -i down || echo none
echo

) | more
