#!/bin/bash
# WARN: DO NOT CRON ME 

set -e

if ! [ "`tty`" != "not a tty" ]; then
   echo "DO NOT CRON ME"
   exit 1
fi

echo -e "\e[1m Please take in mind that there may downtime while executing this task\e[0m"
echo "Only do this if you change services definition (docker-compose) or there's a problem"
read -n 1 -p "Are you sure you want to continue?" answer
[ -z "$answer" ] || exit 0

echo [$0] Grab gcr auth...
gcloud docker --authorize-only

echo [$0] Check stack is deployed...
docker --debug stack deploy -c /root/docker-compose.prod.yml --with-registry-auth prod --resolve-image=always --prune

echo [$0] Pull for new images...
docker-compose --no-ansi -f docker-compose.prod.yml pull --parallel --quiet

echo [$0] Update services...
# loop of two key=value with bash trick
services_images=$(docker service ls --format '{{.Name}}={{.Image}}')
for service_image in $services_images; do
	service=$(echo $service_image | cut -d'=' -f 1)
	image=$(echo $service_image | cut -d'=' -f2)
	echo Updating service $service...
        docker service update -d --with-registry-auth --stop-grace-period 5s --image $image $service ||
           echo -e "\e[1m Oops! \n make sure this service $service is not restarting with ./status.sh \n Then rexecute the deployment\e[0m"
done
